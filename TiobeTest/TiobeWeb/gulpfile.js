﻿/// <binding AfterBuild='compile' />

var gulp =  require("gulp"),
    rimraf = require("rimraf"),
    fs = require("fs"),
    typescript = require('gulp-tsc'),
    bower = require('gulp-bower'),
    uglify = require('gulp-uglify');


eval("var project = " + fs.readFileSync("./project.json"));

var paths = {
  bower: "./bower_components/",
  lib: "./" + project.webroot + "/lib/",
  js: "./" + project.webroot + "/js/"
};

// dist build
gulp.task('build-dist', ['copylibs', 'compress']);

gulp.task('cleanall', ['cleanjs', 'cleanlib', 'cleanbower']);

gulp.task('cleanlib', function (cb) {
  rimraf(paths.lib, cb);
});

gulp.task('cleanbower', function (cb) {
    rimraf(paths.bower, cb);
});

gulp.task('cleanjs', function (cb) {
    rimraf(paths.js, cb);
});


gulp.task('copylibs', ['cleanlib', 'bower'], function () {
  var bower = {
    "bootstrap": "bootstrap/dist/**/*.{js,map,css,ttf,svg,woff,eot}",
    "bootstrap-touch-carousel": "bootstrap-touch-carousel/dist/**/*.{js,css}",
    "hammer.js": "hammer.js/hammer*.{js,map}",
    "jquery": "jquery/jquery*.{js,map}",
    "jquery-validation": "jquery-validation/jquery.validate.js",
    "jquery-validation-unobtrusive": "jquery-validation-unobtrusive/jquery.validate.unobtrusive.js"
  }

  for (var destinationDir in bower) {
    gulp.src(paths.bower + bower[destinationDir])
      .pipe(gulp.dest(paths.lib + destinationDir));
  }
});

gulp.task('bower', ['cleanbower'], function () {
    return bower()
      .pipe(gulp.dest(paths.bower))
});

gulp.task('compile', ['cleanjs'], function () {
    gulp.src(['App/**/*.ts'])
      .pipe(typescript())
      .pipe(gulp.dest(paths.js))
});

gulp.task('compress', ['compile'], function () {
    return gulp.src(paths.js + '*.js')
      .pipe(uglify())
      .pipe(gulp.dest(paths.js));
});
