/// <reference path='_all.ts' />

/**
 * The main TodoMVC app module.
 *
 * @type {angular.Module}
 */
module todos {
    'use strict';

    var todomvc = angular.module('todoApp', [])
        .controller('todoCtrl', TodoCtrl);

    function TodoCtrl() {

    }
}