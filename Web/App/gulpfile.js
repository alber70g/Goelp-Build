var gulp = require('gulp'),
    del = require('del'),
    $ = require('gulp-load-plugins')({ lazy: true }),
    ts = require('gulp-typescript'),
    concat = require('gulp-concat'),
    less = require('gulp-less'),
    sourcemaps = require('gulp-sourcemaps'),
    bower = require('gulp-bower'),
    livereload = require('gulp-livereload'),
    karma = require('karma').server,
    wiredep = require('wiredep').stream;

var config = require('./gulp.config')();

gulp.task('default', function() {
	gulp.start('compile');
	gulp.start('test');
	gulp.start('watch');
});

gulp.task('cleanall', ['cleanjs', 'cleanlib', 'cleancss', 'cleanbower']);

gulp.task('cleanlib', function (cb) {
    del(config.lib, cb);
});

gulp.task('cleancss', function (cb) {
    del(config.css.path, cb);
});

gulp.task('cleanbower', function (cb) {
  del(config.bower.path, cb);
});

gulp.task('cleanjs', function (cb) {
  del(config.typescript.js, cb);
});

gulp.task('less', function () {
    return gulp.src(config.less.lessFile)
      .pipe(less({}))
      .pipe(gulp.dest(config.less.cssDir))
      .pipe(livereload());
});

gulp.task('copylibs', ['cleanlib', 'bower'], function () {
  var bower = {
    "angular": "angular/angular.js",
  }

  for (var destinationDir in bower) {
    gulp.src(config.bower.path + bower[destinationDir])
      .pipe(gulp.dest(config.lib + destinationDir));
  }
});

gulp.task('bower', ['cleanbower'], function () {
    return bower()
        .pipe(gulp.dest(config.bower.path));
});

gulp.task('bowerupdate', function () {
    return bower({ cmd: 'update' })
        .pipe(gulp.dest(config.bower.path));
});

gulp.task('compile', ['cleanjs'], function () {
  var tsResult = gulp.src(config.typescript.ts)
    .pipe(sourcemaps.init())
    .pipe(ts({
        noImplicitAny: true,
        out: config.typescript.js
      }));

  return tsResult.js
    .pipe(concat('output.js'))
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('dist'))
  .pipe(livereload());
});

gulp.task('compress', ['compile'], function () {
    return gulp.src(config.typescript.js + '*.js')
      .pipe(uglify())
      .pipe(gulp.dest(config.typescript.min.js));
});

gulp.task('test', function (done) {
	karma.start({
		configFile: __dirname + '/karma.config.js',
		singleRun: true,
		browsers: ['PhantomJS']
	}, done);
});

gulp.task('watch', ['compile'], function () {
  livereload.listen();
  gulp.watch('src/**/*.less', ['less']);
  gulp.watch('src/**/*.ts', ['compile', 'test']);
});

gulp.task('dependencies', function () {
    return gulp
        .src(config.index)
        .pipe(wiredep(config.getWiredepDefaultOptions()))
        .pipe($.inject(gulp.src(config.typescript.js)))
        .pipe(gulp.dest(config.client));
});