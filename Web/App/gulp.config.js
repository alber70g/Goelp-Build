﻿module.exports = function () {
    var client = 'src/';
    var clientApp = client + '';
    var root = './';
    var temp = './.tmp/';
    var dist = './dist/';

    var wiredep = require('wiredep');
    var bowerFiles = wiredep({ devDependencies: true })['js'];

    var config = {
        lib: './lib/',
        typescript: {
            js: 'app.js',
            ts: ['definitions/*.ts', client + '**/*.ts'],
            min: {
                js: 'app.min.js'
            }
        },
        alljs: [
            './src/**/*.js',
            './*.js'
        ],
        build: './build/',
        client: client,
        css: [
            temp + 'styles.css',
            client + 'styles/**/*.css'
        ],
//        fonts: './bower_components/font-awesome/fonts/**/*.*',
//        html: client + '**/*.html',
//        htmltemplates: clientApp + '**/*.html',
//        images: client + 'images/**/*.*',
        index: client + 'index.html',
        // app js, with no specs
//        js: [
//             clientApp + '**/*.module.js',
//             clientApp + '**/*.js',
//             '!' + clientApp + '**/*.spec.js'
//        ],
        less: {
            lessFile: client + 'app.less',
            cssDir: dist + 'css'
        },
        // report: report,
        root: root,
        source: 'src/',
        temp: temp,

        /**
         * optimized files
         */
        optimized: {
            app: '../dist/app.js',
            lib: '../dist/lib.js'
        },
        
        /**
         * Bower and NPM locations
         */
        bower: {
            json: require('./bower.json'),
            path: client + '/bower_components/',
            ignorePath: '../'
        }
    };

    /**
     * wiredep and bower settings
     */
    config.getWiredepDefaultOptions = function () {
        var options = {
            bowerJson: config.bower.json,
            directory: config.bower.directory,
            ignorePath: config.bower.ignorePath
        };
        return options;
    };


    return config;
}