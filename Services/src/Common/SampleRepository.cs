﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.DirectoryServices;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using Microsoft.Framework.Logging;
using Oracle.ManagedDataAccess.Types;

namespace Common
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class SampleRepository
    {
        private OracleConnection _conn;
        private ILogger _logger;

        public SampleRepository(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<SampleRepository>();

            var connString = @"Data Source=" + ResolveServiceNameLdap("ttsd01pw") + @";User Id=RDIJ1;Password=geheim;";
            _conn = new OracleConnection(connString);
        }

        public void OpenCloseConnection()
        {
            _conn.Open();
            _logger.LogInformation("Connected to Oracle" + _conn.ServerVersion);
            _conn.Close();
        }

        public void ReadFromOracle()
        {
            _conn.Open();
            string sql = "select distinct owner from sys.all_objects order by owner";
            using (OracleCommand comm = new OracleCommand(sql, _conn))
            {
                using (OracleDataReader rdr = comm.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        Console.WriteLine(rdr.GetString(0));
                    }
                }

            }
            _conn.Close();
        }

        public void ExecStoredProc()
        {
            _conn.Open();
            OracleCommand cmd = new OracleCommand("MYPACK.MYSP", _conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            OracleParameter param1 = cmd.Parameters.Add("param1", OracleDbType.Varchar2);
            OracleParameter param2 = cmd.Parameters.Add("param2", OracleDbType.Varchar2);

            param1.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
            param2.CollectionType = OracleCollectionType.PLSQLAssociativeArray;

            param1.Direction = System.Data.ParameterDirection.Input;
            param2.Direction = System.Data.ParameterDirection.Output;

            param1.Value = new string[3] { "Value1", "Value2", "Value3" };
            param2.Value = null;

            param1.Size = 3;
            param2.Size = 3;

            param1.ArrayBindSize = new int[3] { 20, 20, 20 };
            param2.ArrayBindSize = new int[3] { 20, 20, 20 };

            cmd.ExecuteNonQuery();

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine((param2.Value as OracleString[])[i]);
            }

            _conn.Close();
        }

        private string ResolveServiceNameLdap(string serviceName)
        {
            string tnsAdminPath = Path.Combine(@"C:\Oracle\product\11.2.0\client_1\network\admin", "ldap.ora");
            string connectionString = string.Empty;

            // ldap.ora can contain many LDAP servers
            IEnumerable<string> directoryServers = null;

            if (File.Exists(tnsAdminPath))
            {
                string defaultAdminContext = string.Empty;

                using (var sr = File.OpenText(tnsAdminPath))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null)
                    {
                        // Ignore commetns
                        if (line.StartsWith("#"))
                        {
                            continue;
                        }

                        // Ignore empty lines
                        if (line == string.Empty)
                        {
                            continue;
                        }

                        // If line starts with DEFAULT_ADMIN_CONTEXT then get its value
                        if (line.StartsWith("DEFAULT_ADMIN_CONTEXT"))
                        {
                            defaultAdminContext = line.Substring(line.IndexOf('=') + 1).Trim(new[] { '\"', ' ' });
                        }

                        // If line starts with DIRECTORY_SERVERS then get its value
                        if (line.StartsWith("DIRECTORY_SERVERS"))
                        {
                            string[] serversPorts = line.Substring(line.IndexOf('=') + 1).Trim(new[] { '(', ')', ' ' }).Split(',');
                            directoryServers = serversPorts.SelectMany(x =>
                            {
                                // If the server includes multiple port numbers, this needs to be handled
                                string[] serverPorts = x.Split(':');
                                if (serverPorts.Count() > 1)
                                {
                                    return serverPorts.Skip(1).Select(y => string.Format("{0}:{1}", serverPorts.First(), y));
                                }

                                return new[] { x };
                            });
                        }
                    }
                }

                // Iterate through each LDAP server, and try to connect
                foreach (string directoryServer in directoryServers)
                {
                    // Try to connect to LDAP server with using default admin contact
                    try
                    {
                        var directoryEntry = new DirectoryEntry("LDAP://" + directoryServer + "/" + defaultAdminContext, null, null, AuthenticationTypes.Anonymous);
                        var directorySearcher = new DirectorySearcher(directoryEntry, "(&(objectclass=orclNetService)(cn=" + serviceName + "))", new[] { "orclnetdescstring" }, SearchScope.Subtree);

                        SearchResult searchResult = directorySearcher.FindOne();

                        var value = searchResult.Properties["orclnetdescstring"][0] as byte[];

                        if (value != null)
                        {
                            connectionString = Encoding.Default.GetString(value);
                        }

                        // If the connection was successful, then not necessary to try other LDAP servers
                        break;
                    }
                    catch
                    {
                        // If the connection to LDAP server not successful, try to connect to the next LDAP server
                        continue;
                    }
                }

                // If casting was not successful, or not found any TNS value, then result is an error message
                if (string.IsNullOrEmpty(connectionString))
                {
                    connectionString = "TNS value not found in LDAP";
                }
            }
            else
            {
                // If ldap.ora doesn't exist, then return error message
                connectionString = "ldap.ora not found";
            }

            return connectionString;
        }
    }
}
